# Simple2DEngine ( WIP )
Just a super simple 2D engine using SDL2 and C++.

## Building

This can only be built in linux systems, but I'll probably add windows and mac support.

### Dependencies

The dependencies are SDL2 and SDL2_image those can be installed by running `sudo apt install libsdl2-2.0-0 libsdl2-image-2.0-0`

Use `make` to compile the program.

