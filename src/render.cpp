#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <sstream>
#include "render.h"
#include "textures.h"
#include "entity.h";
#include "vector.h";

RenderWindow::RenderWindow(const char* title, int sizex, int sizey){
	if(SDL_Init(SDL_INIT_VIDEO) > 0)
		printf("Error ! Couldn't Initialize Video %d\n", SDL_GetError());

	Window = SDL_CreateWindow(title,SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sizex, sizey, SDL_WINDOW_SHOWN);

	if(Window == NULL) 
		printf("Couldn't create window ! Error : %d \n ", SDL_GetError());

	Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED);
}

void RenderWindow::close(){
    SDL_DestroyWindow( Window );
    SDL_DestroyRenderer( Renderer );
    Window = NULL;
    Renderer = NULL;

    SDL_Quit();

}

void RenderWindow::update() {

}

SDL_Renderer* RenderWindow::getRenderer(){
	return Renderer;
}

void RenderWindow::clear() {
	SDL_SetRenderDrawColor( Renderer, 0x00, 0x00, 0x00, 0x00 );
	SDL_RenderClear(Renderer);
}


void RenderWindow::display(){
	SDL_RenderPresent(Renderer);
}

void RenderWindow::render(Texture* tex, Vec2 pos){


	SDL_Rect sourceRect;
	sourceRect.x = sourceRect.y = 0;
	sourceRect.w = tex->getWidth();
	sourceRect.h = tex->getHeight();

	SDL_Rect destinationRect;
	destinationRect.x = pos.x;
	destinationRect.y = pos.y;
	destinationRect.w = tex->getWidth();
	destinationRect.h = tex->getHeight();
	if(SDL_RenderCopy(Renderer, tex->getTexture(), &sourceRect, &destinationRect)!=0){
		printf("Couldn't render texture! : %s\n", SDL_GetError() );
	}
}

void RenderWindow::render(Entity ent){
	SDL_Rect frame = ent.getFrame();
	SDL_Rect sourceRect;
	sourceRect.x = frame.x;
	sourceRect.y = frame.y;
	sourceRect.w = frame.w;
	sourceRect.h = frame.h;

	SDL_Rect destinationRect;
	destinationRect.x = ent.getPos().x;
	destinationRect.y = ent.getPos().y;
	destinationRect.w = frame.w;
	destinationRect.h = frame.h;



	SDL_Texture* texture = ent.getSDLTexture();

	if(texture!=NULL){
		SDL_RenderCopy(Renderer, texture, &sourceRect, &destinationRect);
	}else{
		SDL_SetRenderDrawColor( Renderer, 0x00, 0x00, 0xFF, 0xFF );   
		if(SDL_RenderFillRect(Renderer, &destinationRect) < 0){
			printf("Error while drawing rect! %s\n", SDL_GetError());

		}
	}
}


void RenderWindow::render(SDL_Texture* tex, SDL_Rect* tex_rect){
	if(SDL_RenderCopy(Renderer, tex, NULL, tex_rect)!=0){
		printf("Error while rendering text! %s \n", SDL_GetError());
	}
}
