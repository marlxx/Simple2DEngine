#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "entity.h"
#include "render.h"
#include "textures.h"
#include "vector.h"

// Entity initalization
Entity::Entity(Vec2 e_pos, Texture* e_texture){
	entityTexture = e_texture;
	pos = e_pos;
	frame.x = 0;
	frame.y = 0;
	frame.w = entityTexture->getWidth();
	frame.h = entityTexture->getHeight();
}

Entity::Entity(SDL_Rect* e_rect){
	entityTexture = NULL;
	pos.x = e_rect->x;
	pos.y = e_rect->y;
	frame.x = 0;
	frame.y = 0;
	frame.w = e_rect->w;
	frame.h = e_rect->h;
}

Vec2 Entity::getPos(){
	return pos;
}

void Entity::setPos(Vec2 e_pos){
	pos = e_pos;
}

int Entity::getId(){
	return id;
}

SDL_Rect Entity::getFrame(){
	return frame;
}

Texture* Entity::getTexture(){
	return entityTexture;
}

SDL_Texture* Entity::getSDLTexture(){
	if(entityTexture!=NULL)
		return entityTexture->getTexture();
	else
		return NULL;
}


