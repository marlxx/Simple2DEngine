#include <stdio.h>
#include <string>
#include <sstream>
#include <stdint.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "timer.h"


Timer::Timer(){
    StartTime = 0;
    PausedTime = 0;

    Paused = false;
    Started = false;
}

void Timer::start(){
	StartTime = SDL_GetTicks();
	EndTime = 0;

	Started = true;
}

void Timer::stop(){
	EndTime = SDL_GetTicks();
}

void Timer::pause(){
	
}

void Timer::unpause(){
	
}

uint32_t Timer::getTime(){
	uint32_t time = 0;

	if(Started){
		if(!Paused){
			return (EndTime - StartTime);
		}else{
			return PausedTime;
		}
	}
	
}