#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <stdlib.h> 
#include "render.h"
#include "textures.h"
#include "entity.h"
#include "timer.h"
#include "vector.h"
#include "text.h"

const int WINDOW_FPS = 60;
const int WINDOW_SIZEX = 640;
const int WINDOW_SIZEY = 480;

using namespace std;

void cleanup();

bool running = true;
SDL_Event e;
Timer timer;
float elapsedTime;
float fpsTime = 1/WINDOW_FPS*1000;
Vec2 pos;
Texture text;

vector<Entity> entities;
vector<Text> textObjects;

SDL_Color textColor = { 255, 255, 255 };
SDL_Surface *message = NULL;

TTF_Font *defaultFont = NULL;
uint64_t ticks;
bool loadAssets (RenderWindow* window){
	
	printf("Loading font ... \n");
	defaultFont = TTF_OpenFont( "assets/sans.ttf", 12 );
	if( defaultFont == NULL )
	{
		printf( "Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError() );
		return false;
	}
	
	printf("Loading textures ... \n");
	if(!text.loadFromRenderedText(window->getRenderer(), "Tick x", defaultFont, textColor)){
		printf("Failed to generate texture from text.\n");
		return false;
	}

	printf("Creating entities ...\n");
	Texture tex1;
	tex1.loadFromFile(window->getRenderer(),"assets/test.png");
	return true;
}

void createObjects(RenderWindow* window){
	// Generate text objects
	int limit = 50;
	printf("%d Starting to create %d objects\n", ticks, limit);
	/*
	for(int b = 0; b<limit; b++){
		
		static Text tex;
		char text[20];
		sprintf(text, "%02d", rand() % 100);
		printf(text);
		
		if(!tex.loadFromRenderedText(window->getRenderer(), text, defaultFont, textColor)){
			printf("Failed to generate texture from text.\n");
		}
		tex.pos = Vec2(b*tex.getWidth(),b/limit);
		textObjects.push_back(tex);
	}
	*/
	static Text tex;
	char text[20];
	sprintf(text, "Tick %010d", ticks);
	printf(text);
		
	if(!tex.loadFromRenderedText(window->getRenderer(), text, defaultFont, textColor)){
		printf("Failed to generate texture from text.\n");
	}
	tex.pos = Vec2(1,10);
	textObjects.push_back(tex);
}

void clear(){
	for(Text obj : textObjects){
		obj.free();
		textObjects.pop_back();
	}
}

void render(RenderWindow* window){
	printf("%d Recreating text objects\n",ticks);
	//createObjects(window);
	
	//window->render(&text,Vec2(0,0));
	printf("%d Render entities\n",ticks);
	for(Entity ent : entities){
		window->render(ent);
	}
	printf("%d Render textures\n",ticks);
	for(Text text : textObjects){
		window->render(&text, text.pos);
	}
}

int main (int argv, char* args[]) {

	if( TTF_Init() == -1 )
    {
    	printf("Error %s",TTF_GetError());
        return 0;    
    }

    RenderWindow window("Example",WINDOW_SIZEX, WINDOW_SIZEY);

    printf("%d Loading assets\n", ticks);
    loadAssets(&window);
    printf("%d Creating objects\n", ticks);
    createObjects(&window);

	while (running) { 
		while ( SDL_PollEvent(&e) != 0) { 
			if( e.type == SDL_QUIT ){
				running = false;	
			}
		}

		timer.start(); // Get Start time
		SDL_SetRenderDrawColor( window.getRenderer(), 0xFF, 0xFF, 0xFF, 0xFF );
		printf("%d Window clear\n",ticks);
		window.clear();
		clear();

		createObjects(&window);
		render(&window);
		
		printf("%d Render Display\n",ticks);
		window.display();
		timer.stop(); // Get end time
		elapsedTime = timer.getTime(); // End - Start Time
		ticks++;
		SDL_Delay(floor(16.666f - elapsedTime));
	}


	cleanup();
	window.close();
	return 0;

}

void cleanup(){
	TTF_CloseFont( defaultFont );
    TTF_Quit();
	
}