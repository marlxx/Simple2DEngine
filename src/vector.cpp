#include <stdio.h>
#include <vector.h>

Vec2::Vec2(){
	x = y = 0;
}

Vec2::Vec2(float vx, float vy){
	x = vx;
	y = vy;
}

Vec2::~Vec2(){
	x = y = NULL;
}

Vec2 Vec2::operator+(const Vec2& vec2){
	Vec2 vec;
	vec.x = this->x + vec2.x;
	vec.y = this->y + vec2.y;
	return vec;
}

Vec2 Vec2::operator-(const Vec2& vec2){
	Vec2 vec;
	vec.x = this->x - vec2.x;
	vec.y = this->y - vec2.y;
	return vec;
}

Vec2 Vec2::operator*(const Vec2& vec2){
	Vec2 vec;
	vec.x = this->x * vec2.x;
	vec.y = this->y * vec2.y;
	return vec;
}

Vec2 Vec2::operator/(const Vec2& vec2){
	Vec2 vec;
	vec.x = this->x / vec2.x;
	vec.y = this->y / vec2.y;
	return vec;
}

Vec3::Vec3(){
	x = y = z = 0;
}

Vec3::~Vec3(){
	x = y = z = NULL;
}

Vec3::Vec3(float vx, float vy, float vz){
	x = vx;
	y = vy;
	z = vz;
}

Vec3 Vec3::operator+(const Vec3& vec3){
	Vec3 vec;
	vec.x = this->x + vec3.x;
	vec.y = this->y + vec3.y;
	vec.z = this->z + vec3.z;
	return vec;
}

Vec3 Vec3::operator-(const Vec3& vec3){
	Vec3 vec;
	vec.x = this->x - vec3.x;
	vec.y = this->y - vec3.y;
	vec.z = this->z - vec3.z;
	return vec;
}

Vec3 Vec3::operator*(const Vec3& vec3){
	Vec3 vec;
	vec.x = this->x * vec3.x;
	vec.y = this->y * vec3.y;
	vec.z = this->z * vec3.z;
	return vec;
}

Vec3 Vec3::operator/(const Vec3& vec3){
	Vec3 vec;
	vec.x = this->x / vec3.x;
	vec.y = this->y / vec3.y;
	vec.z = this->z / vec3.z;
	return vec;
}


