#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "textures.h"
#include "render.h"
#include "vector.h"

Texture::Texture(){
	texture = NULL;

}


Texture::~Texture(){
	printf("Free Texture\n");
	free();
}

void Texture::free(){
	// Destroy texture
	if(texture!=NULL){
		SDL_DestroyTexture(texture);
		texture = NULL;
		width, height = NULL;
	}	
}

bool Texture::loadFromFile(SDL_Renderer* Renderer, const char* path) {
	texture = IMG_LoadTexture(renderer, path);

	if(texture == NULL){
		printf("Couldn't load texture %s! Error : %s", path, SDL_GetError());
		return false;
	}
	
	SDL_QueryTexture(texture,NULL,NULL,&width,&height);
	printf("Texture width and height : %s %s", width, height);
	return true;
}

bool Texture::loadFromRenderedText(SDL_Renderer* Renderer, const char* text, TTF_Font* font, SDL_Color color){
	printf("Started to generate texture\n");
	if(Renderer == NULL){
		printf("Define a renderer first. \n");
		return false;
	}
	printf("Creating a surface\n");
	SDL_Surface* Surface = TTF_RenderText_Solid(font, text, color);
	if(Surface == NULL){
		printf("Error while making surface %s\n", TTF_GetError());
		return false;
	}
	printf("Creating a texture \n");
	texture = SDL_CreateTextureFromSurface(Renderer, Surface);
	if(texture == NULL){
		printf("Error while generating texture %s\n", SDL_GetError());
		return false;
	}
	printf("penis\n");
	SDL_Rect* rect;
	width = Surface->w;
	height = Surface->h;
	printf("penis\n");
	Surface = NULL; // Destroy surface

	return true;

}

SDL_Rect* Texture::getRect(){
	SDL_Rect* rect;
	rect->x = rect->y = 0;
	rect->w = width;
	rect->h = height;
	return rect;
}

SDL_Rect* Texture::getRect(Vec2 pos){
	SDL_Rect* rect;
	rect->x = pos.x;
	rect->y = pos.y;
	rect->w = width;
	rect->h = height;
	return rect;
}

void Texture::setColor(uint8_t red, uint8_t green, uint8_t blue){
	SDL_SetTextureColorMod(texture, red, green, blue);
}

SDL_Texture* Texture::getTexture(){
	if(texture==NULL){printf("Warning : Texture does not exist\n");}
	return texture;
}

void Texture::scale(int x,int y){
	width = width*x;
	height = height*y;
}

int Texture::getWidth(){
	return width;
}


int Texture::getHeight(){
	return height;
}