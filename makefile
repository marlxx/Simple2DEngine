ODIR = src
IDIR = include

_OBJ = main.cpp render.cpp timer.cpp textures.cpp entity.cpp vector.cpp 
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJ))

_DEPS = render.h timer.h textures.h entity.h vector.h text.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

CC = g++

INCLUDE=-I./include

COMPILER_FLAGS = -w 

COMPILER_DEBUG_FLAGS = -w -g

LINKER_FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf

OBJ_NAME = main

all : $(OBJS)
	$(CC) $(INCLUDE) $(OBJS) $(DEPS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

debug : $(OBJS)
	$(CC) $(INCLUDE) $(OBJS) $(DEPS) $(COMPILER_DEBUG_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME) 