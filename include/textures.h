#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "vector.h"

typedef class Texture{
	public:
		Texture();
		~Texture(); // Deallocate RAM

		bool loadFromFile(SDL_Renderer* Renderer, const char* path);
		bool loadFromRenderedText(SDL_Renderer* Renderer, const char* text, TTF_Font* font, SDL_Color color);

		void setColor(uint8_t red, uint8_t green, uint8_t blue);

		void setAlpha(uint8_t amount);

		void scale(int x,int y);

		int getWidth();
		int getHeight();
		SDL_Rect* getRect();
		SDL_Rect* getRect(Vec2 pos);
		
		void free();

		SDL_Texture* getTexture(); 
	private:
		SDL_Renderer* renderer;
		SDL_Texture* texture;
		int width, height;
};

