#pragma once

#include <stdint.h>
#include <stdio.h>


class Timer
{
    public:
		Timer();

		void start();
		void stop();
		void pause();
		void unpause();

		uint32_t getTime();

		bool isStarted();
		bool isPaused();

    private:
		uint32_t StartTime;
		uint32_t PausedTime;
		uint32_t EndTime;

		bool Paused;
		bool Started;
};
