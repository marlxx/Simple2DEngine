#pragma once
#include <stdio.h>

class Vec2{
	public:
		Vec2();
		~Vec2();
		Vec2(float vx, float vy);
		Vec2 operator+(const Vec2& vec2);
		Vec2 operator-(const Vec2& vec2);
		Vec2 operator*(const Vec2& vec2);
		Vec2 operator/(const Vec2& vec2);


		float x;
		float y;
};

class Vec3{
	public:
		Vec3();
		~Vec3();
		Vec3(float vx, float vy, float vz);
		Vec3 operator+(const Vec3& vec3);
		Vec3 operator-(const Vec3& vec3);
		Vec3 operator*(const Vec3& vec3);
		Vec3 operator/(const Vec3& vec3);
		
		float x;
		float y;
		float z;
};
