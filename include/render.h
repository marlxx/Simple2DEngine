#pragma once

#include <SDL2/SDL.h>
#include <sstream>
#include "timer.h"
#include "textures.h"
#include "entity.h"
#include "vector.h"


class RenderWindow{

	public:
		
		RenderWindow(const char* title, int sizex, int sizey);
		void update();
		void close();
		void display();
		void clear();

		void render(SDL_Texture* texture, SDL_Rect* tex_rect);
		void render(Texture* tex, Vec2 pos);
		void render(Entity ent);

		SDL_Renderer* getRenderer();

	private:
		SDL_Window* Window;
		SDL_Renderer* Renderer;
		Timer fpsTimer;
		Timer capTimer;
};
