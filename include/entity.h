#ifndef CAMERA_H
#define CAMERA_H

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "textures.h"
#include "vector.h"


class Texture;

class Entity{
	public:
		Entity(Vec2 e_pos, Texture* e_texture);
		Entity(SDL_Rect* e_rect);
		Vec2 getPos();
		void setPos(Vec2 e_pos);
		SDL_Rect getFrame();
		Texture* getTexture();
		SDL_Texture* getSDLTexture();
		int getId();
	private:
		int id;
		Vec2 pos;
		SDL_Rect frame;
		Texture* entityTexture;

};

#endif